// This Google Script searchs on your Gmail account, and store the results on a Google Sheet file
// Original: https://github.com/TiagoGouvea/gmail-to-google-sheets-script/

// Add here your search query. Do your search on gmail first, copy and paste the search terms here
// Samples: "label: hiring-process", "to: sales@mycompany.com"
var SEARCH_QUERY = 'after:2023-06-01 AND (to:csi@fanime.com OR to:policy@fanime.com OR to:attendee.relations@fanime.com)';
// If you want each email address just once on sheet, set to true
var AVOID_REPEATED_ADDRESS = false;
var AVOID_DRAFTS = true;
var PAGE_SIZE = 500;
var ROW_HEIGHT = 25;

// Main function, the one that you must select before run
function saveEmails() {
    var sheet = SpreadsheetApp.getActiveSheet();
    var existingIds = new Set(sheet.getRange(2, 1, sheet.getMaxRows()).getDisplayValues().flat());
  
    console.log(`Searching for: "${SEARCH_QUERY}"`);
    var pageStart = 0;
    
    var threads = GmailApp.search(SEARCH_QUERY, pageStart, PAGE_SIZE);
    if (threads != null){
      console.log("Threads found 🎉");
      console.log("Paginating to collect email addresses...");
    } else {
      console.warn("No emails found within search criteria 😢");
      return;
    }

    var header = [["ID", "Date", "From", "To", "Subject", "Link", "Body"]];
    sheet.getRange(1, 1, header.length, header[0].length).setValues(header);
    
    var totalEmails = 0;
    var emails = [];
    var addresses = [];
    while (threads.length>0){
      var batchSkipped = 0;
      for (var i in threads) {
          var thread=threads[i];
          var date = thread.getLastMessageDate();
          var msgs = threads[i].getMessages();
          for (var j in msgs) {
            var msg = msgs[j];
            var msgId = msg.getId();

            // Values to get and store ✏️
            var date = msg.getDate().toISOString();          
            var from = msg.getFrom();
            var to = msg.getTo();
            var subject = msg.getSubject();
            var link = thread.getPermalink();
            var body = msg.getPlainBody();
            var dataLine = [msgId, date, from, to, subject, link, body];

            // Add values to array
            if (!existingIds.has(msgId) && 
              (!AVOID_REPEATED_ADDRESS || !addresses.includes(to)) &&
              (!AVOID_DRAFTS || !msg.isDraft())
            ){
              emails.push(dataLine);
              addresses.push(from);
              existingIds.add(msgId);
              existingIds.add(msgId);
            } else {
              batchSkipped++;
            }
          }
        }

      totalEmails = totalEmails + emails.length;

      // Add emails to sheet
      if(emails.length > 0) {
        appendData(emails);
      }
      console.log("Adding %d emails to sheet, %d where skipped as duplicate", emails.length, batchSkipped);

      if (threads.length == PAGE_SIZE){
          console.log("Reading next page...");
      } else {
          console.log("Last page read 🏁");
      }
      pageStart = pageStart + PAGE_SIZE; 
      threads = GmailApp.search(SEARCH_QUERY, pageStart, PAGE_SIZE);
    }

    console.info("%d total emails added to sheet 🎉", totalEmails);
    sheet.setRowHeightsForced(1, sheet.getMaxRows(), ROW_HEIGHT);

    console.info("Sheet should now have %d entries", existingIds.size);
}

// Add contents to sheet
function appendData(array2d) {
  var sheet = SpreadsheetApp.getActiveSheet();
  sheet.insertRowsAfter(1, array2d.length);
  sheet.getRange(2, 1, array2d.length, array2d[0].length).setValues(array2d);
}